<?xml version="1.0"?>

<project name="trainingunit" basedir="." default="usage">

	<property name="name" value="trainingunit"/>

	<property name="war.dir" value="war"/>
	<property name="src.dir" value="src"/>
	<property name="lib.dir" value="lib"/>
	<property name="webclasses.dir" value="${war.dir}/WEB-INF/classes"/>
	<property name="weblib.dir" value="${war.dir}/WEB-INF/lib"/>
	<property name="build.dir" value=".classes"/>
	<property name="db.dir" value="db"/>

	<property name="domain.classes" value="org/trainingunit/*.class"/>
	
	<property name="testsrc.dir" value="test"/>
	<property name="testbuild.dir" value=".testclasses"/>
	<property name="testreports.dir" value="testng-reports"/>
	<property name="testhtml.dir" value="${testreports.dir}/html"/>

	<property file="build.properties"/>

	<path id="master-classpath">
		<fileset dir="${lib.dir}">
			<include name="*.jar"/>
		</fileset>
	</path>

	<path id="build-classpath">
		<fileset dir="${lib.dir}">
			<include name="*.jar"/>
		</fileset>
	</path>

	<path id="test-classpath">
		<fileset dir="${lib.dir}">
			<include name="*.jar"/>
		</fileset>
		<pathelement location="${testbuild.dir}"/>
	</path>
<!--		<pathelement location="${lib.dir}/*.jar"/>	-->


	<target name="usage">
		<echo message=""/>
		<echo message="PetClinic build file"/>
		<echo message="------------------------------------------------------"/>
		<echo message=""/>
		<echo message="Available targets are:"/>
		<echo message=""/>
		<echo message="clean     --> Clean output dirs"/>
		<echo message="build     --> Compile main Java sources and copy libraries"/>
		<echo message="docs      --> Create complete Javadoc documentation"/>
		<echo message="warfile   --> Build the web application archive"/>
		<echo message="deploywar --> Deploy the web application archive"/>
		<echo message="setupDB   --> Initialize the database"/>
		<echo message="tests     --> Run tests using initialized database"/>
		<echo message="all       --> Clean, build, docs, warfile, tests"/>
		<echo message=""/>
	</target>

	<target name="clean" description="Clean output dirs (build, docs, testbuild, testreports, weblib, dist)">
		<delete dir="${build.dir}"/>
		<delete dir="${docs.dir}"/>
		<delete dir="${weblib.dir}"/>
		<delete>
			<fileset dir="${webclasses.dir}">
				<include name="*.hbm.xml"/>
				<include name="jdbc.properties"/>
			</fileset>
		</delete>
		<delete dir="${testbuild.dir}"/>
		<delete dir="${testreports.dir}"/>
		<delete dir="${dist.dir}"/>
	</target>

	<target name="build" description="Compile main source tree java files into class files, generate jar files">
		
		<mkdir dir="${build.dir}"/>

		<javac destdir="${build.dir}" source="1.5" target="1.5"
				debug="true" deprecation="false" optimize="false" failonerror="true">
			<src path="${src.dir}"/>
			<classpath refid="master-classpath"/>
		</javac>

		<!-- trainingunit-domain.jar -->
		<jar jarfile="${weblib.dir}/${name}-domain.jar" compress="true">
			<fileset dir="${src.dir}"/>
			<fileset dir="${build.dir}">
				<include name="${domain.classes}"/>
			</fileset>
		</jar>

		<!-- rest of PetClinic classes -->
		<jar jarfile="${weblib.dir}/${name}.jar" compress="true">
			<fileset dir="${src.dir}">
				<include name="META-INF/aop.xml"/>
			</fileset>
			<fileset dir="${build.dir}">
				<include name="**/*"/>
				<exclude name="${domain.classes}"/>
			</fileset>
		</jar>

	</target>

	<target name="docs" description="Create complete Javadoc documentation">

		<delete dir="${docs.dir}"/>
		<mkdir dir="${docs.dir}"/>

		<javadoc executable="${javadoc.exe}" 
		        sourcepath="${src.dir}" destdir="${docs.dir}" windowtitle="PetClinic"
				source="1.5" author="true" version="true" use="true" packagenames="*">
			<doctitle><![CDATA[<h1>PetClinic</h1>]]></doctitle>
			<bottom><![CDATA[<i>Ken Krebs and Juergen Hoeller, 2003-2007.</i>]]></bottom>
			<classpath refid="master-classpath"/>
			<classpath refid="build-classpath"/>
		</javadoc>

	</target>

	<target name="warfile" depends="build,docs" description="Build the web application archive">

		<mkdir dir="${dist.dir}"/>

		<war warfile="${dist.dir}/${name}.war" basedir="${war.dir}" webxml="${war.dir}/WEB-INF/web.xml">
			<include name="*"/>
			<include name="docs/**"/>
			<include name="html/**"/>
			<include name="styles/**"/>
			<include name="images/**"/>
			<include name="WEB-INF/*.*"/>
			<exclude name="WEB-INF/web.xml"/>
			<include name="WEB-INF/classes/*.*"/>
			<include name="WEB-INF/lib/**"/>
			<include name="WEB-INF/jsp/**"/>
			<include name="WEB-INF/classes/META-INF/*"/>
			<include name="META-INF/*"/>
			<exclude name="**/.*"/>
			<exclude name="WEB-INF/geronimo-web.xml"/>
		</war>

	</target>

    <target name="deploywar" depends="warfile" description="Deploy application as a WAR file">
        <copy todir="${deploy.path}" preservelastmodified="true">
            <fileset dir="${dist.dir}">
                <include name="*.war"/>
            </fileset>
        </copy>
    </target>
	

	<!--
		If you are going to deploy the application into Geronimo (or an application server
		variant of Geronimo), run this target in preference to the plain 'warfile'.
		This is required in order to avoid Geronimo class loading issues.
	-->
	<target name="warfile-geronimo" depends="build,docs" description="Build the web application archive packaged specifically for Geronimo">

		<mkdir dir="${dist.dir}"/>

		<war warfile="${dist.dir}/${name}.war" basedir="${war.dir}" webxml="${war.dir}/WEB-INF/web.xml">
			<include name="*"/>
			<include name="docs/**"/>
			<include name="html/**"/>
			<include name="styles/**"/>
			<include name="images/**"/>
			<include name="WEB-INF/*.*"/>
			<exclude name="WEB-INF/web.xml"/>
			<include name="WEB-INF/classes/*.*"/>
			<include name="WEB-INF/lib/**"/>
			<include name="WEB-INF/jsp/**"/>
			<exclude name="**/.*"/>
			<exclude name="WEB-INF/lib/commons-logging.jar"/>
			<exclude name="WEB-INF/lib/cglib*.jar"/>
		</war>

	</target>


	<target name="setupDB" description="Initialize the database">

		<property name= "db.driver" value= "${mssql.driver}" />
		<property name= "db.url" value= "${mssql.url}" />
		<property name= "db.user" value= "${mssql.user}" />
		<property name= "db.pw" value= "${mssql.pw}" />

		<ant antfile="${db.dir}/build.xml"/>

	</target>

	<target name="tests" depends="build,setupDB" description="Run tests using initialized database">

		<delete dir="${testbuild.dir}"/>
		<mkdir dir="${testbuild.dir}"/>
		<delete dir="${testreports.dir}"/>
		<mkdir dir="${testreports.dir}"/>
		<delete dir="${testhtml.dir}"/>
		<mkdir dir="${testhtml.dir}"/>

		<taskdef resource="testngtasks" classpathref="test-classpath"/>
		
		<javac srcdir="${testsrc.dir}" destdir="${testbuild.dir}" debug="true" deprecation="true">
			<classpath path="${build.dir}"/>
			<classpath refid="master-classpath"/>
		</javac>

		<copy todir="${testbuild.dir}" preservelastmodified="true">
			<fileset dir="${src.dir}">
				<include name="**/*.xml"/>
				<include name="**/*.properties"/>
			</fileset>
			<fileset dir="${testsrc.dir}">
				<include name="**/*.xml"/>
				<include name="**/*.properties"/>
			</fileset>
		</copy>

		<testng classpathref="test-classpath" groups="Important">
			<classfileset dir="${testbuild.dir}" includes="**/*.class"/>
			<classpath location="${build.dir}"/>
		</testng>
<!--
		<junit forkmode="perBatch" printsummary="true" haltonfailure="no" haltonerror="no">
			<classpath path="${build.dir}"/>
			<classpath path="${testbuild.dir}"/>
			<classpath refid="master-classpath"/>
			<classpath refid="test-classpath"/>

			<!-
				<formatter type="plain" usefile="false"/>
			->
			<formatter type="xml"/>

			<batchtest fork="yes" todir="${testreports.dir}">
				<fileset dir="${testbuild.dir}">
					<include name="**/*Tests.class"/>
					<exclude name="**/Abstract*Tests.class"/>
					<exclude name="**/*SuiteTests.class"/>
				</fileset>
			</batchtest>
		</junit>

		<junitreport todir="${testhtml.dir}">
			<fileset dir="${testreports.dir}">
				<include name="TEST-*.xml"/>
			</fileset>
			<report format="frames" todir="${testhtml.dir}"/>
		</junitreport>
-->
		<!-- restore the database -->
		<!--
		<ant antfile="${db.dir}/build.xml"/>
		-->

	</target>


	<target name="all" depends="clean,build,docs,warfile,tests" description="Clean,build,docs,warfile,tests"/>

</project>
