package test;

import java.util.List;
import java.util.Date;

import org.omg.CORBA.PUBLIC_MEMBER;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import static org.testng.AssertJUnit.assertEquals;

import org.tu.Entity;
import org.tu.Course;
import org.tu.Company;
import org.tu.Class;
import org.tu.Student;
import org.tu.Instructor;
import org.tu.TrainingUnit;

public class TrainingUnitTest {

	private TrainingUnit trainingUnit;
	
	@Test(groups = { "init" })
	public void TrainingUnit() {
		trainingUnit = new TrainingUnit(); 
	}
	
	@Test(dependsOnGroups = { "init" }, groups = { "Test" })
	public class InitializedTrainingUnitTest {
		
		@Test
		public void getCompanies() {
			List<Company> companies = trainingUnit.getCompanies();
			assertEquals("Initialy configured number of rows is different", 4, companies.size());
		}

		@Test
		public void getCourses() {
			List<Course> courses = trainingUnit.getCourses();
			assertEquals("Initialy configured number of rows is different", 5, courses.size());
		}

		@Test
		public void getInstructors() {
			List<Instructor> instructors = trainingUnit.getInstructors();
			assertEquals("Initialy configured number of rows is different", 8, instructors.size());
		}

		@Test
		public void getStudents() {
			List<Student> students = trainingUnit.getStudents();
			assertEquals("Initialy configured number of rows is different", 21, students.size());
		}

		@Test
		public void loadEntity() {
			Student s = trainingUnit.loadEntity(Student.class, 7);
			assertEquals("Acquired property differs from expected", "Robert", s.getFirstName());
			assertEquals("Acquired property differs from expected", "John", s.getMiddleName());
			assertEquals("Acquired property differs from expected", "Odenkirk", s.getLastName());
			assertEquals("Acquired property differs from expected", "std14rodenkirk", s.getLogin());
			assertEquals("Acquired property differs from expected", "robertodenkirk2014", s.getPassword());
			
			Company cmp = trainingUnit.loadEntity(Company.class, 3);
			assertEquals("Acquired property differs from expected", "Wannaknow", cmp.getName());
			assertEquals("Acquired property differs from expected", "31 Spooner St.", cmp.getAddress());
			assertEquals("Acquired property differs from expected", "cmp14wannaknow", cmp.getLogin()); 
			assertEquals("Acquired property differs from expected", "wannaknow2014", cmp.getPassword());

			Instructor i = trainingUnit.loadEntity(Instructor.class, 5);
			assertEquals("Acquired property differs from expected", "Henry", i.getFirstName());
			assertEquals("Acquired property differs from expected", "Albert", i.getMiddleName());
			assertEquals("Acquired property differs from expected", "Azaria", i.getLastName());
			assertEquals("Acquired property differs from expected", "ins14hazaria", i.getLogin());
			assertEquals("Acquired property differs from expected", "hazaria2014", i.getPassword());
			assertEquals("Acquired property differs from expected", 3, i.getCompany().getId());

			Course crs = trainingUnit.loadEntity(Course.class, 4);
			assertEquals("Acquired property differs from expected", "003Singing2014", crs.getName());
			assertEquals("Acquired property differs from expected", "You like singing in the shower but not sure if you are good enough for karaoke parties? This course is for you!", crs.getDescription());
			assertEquals("Acquired property differs from expected", 3, crs.getCompany().getId());
			assertEquals("Acquired property differs from expected", new Date(2014 - 1900, 6, 25), crs.getStartDate());
			assertEquals("Acquired property differs from expected", 6, crs.getDuration());

			Class cls = trainingUnit.loadEntity(Class.class, 26);
			assertEquals("Acquired property differs from expected", 4, cls.getCourse().getId());
			assertEquals("Acquired property differs from expected", 2, cls.getDuration());
			assertEquals("Acquired property differs from expected", new Date(2014 - 1900, 6, 25, 17, 20), cls.getDate());
		}

		@Test
		public void storeEntity() {
			Student s1 = new Student();
			s1.setFirstName("Georgy");
			s1.setMiddleName("Nikolaevich");
			s1.setLastName("Moshkin");
			s1.setLogin("std14gmoshkin");
			s1.setPassword("georgymoshkin2014");
			trainingUnit.storeEntity(s1);
			
			Student s2 = trainingUnit.loadEntity(Student.class, 22);
			assertEquals("Acquired property differs from expected", "Georgy", s2.getFirstName());
			assertEquals("Acquired property differs from expected", "Nikolaevich", s2.getMiddleName());
			assertEquals("Acquired property differs from expected", "Moshkin", s2.getLastName());
			assertEquals("Acquired property differs from expected", "std14gmoshkin", s2.getLogin());
			assertEquals("Acquired property differs from expected", "georgymoshkin2014", s2.getPassword());
		}
	}
}
