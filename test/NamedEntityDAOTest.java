package test;

import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertEquals;

import java.lang.AssertionError;

import org.testng.annotations.Test;

import org.hibernate.HibernateException;

import org.tu.Company;
import org.tu.Course;
import org.tu.dao.EntityDAO;
import org.tu.dao.NamedEntityDAO;
import org.tu.util.SessionFactoryDispenser;

@Test(groups = { "Important", "DAO" }, dependsOnGroups = { "SessionFactoryDispenser" })
public class NamedEntityDAOTest {
	@Test
	public void getGoodNamedEntity() {
		NamedEntityDAO e = new NamedEntityDAO();
		Company c = e.getNamedEntity(Company.class, "Learner");
		assertEquals("Acquired property differs from expected", 1, c.getId());
		assertEquals("Acquired property differs from expected", "Learner", c.getName());
		assertEquals("Acquired property differs from expected", "742 Evergreen Terrace", c.getAddress());
		assertEquals("Acquired property differs from expected", "cmp14learner", c.getLogin());
		assertEquals("Acquired property differs from expected", "learner2014", c.getPassword());
	}
	@Test(expectedExceptions = AssertionError.class)
	public void getBadNamedEntity() {
		NamedEntityDAO e = new NamedEntityDAO();
		Company c = e.getNamedEntity(Company.class, "Gazprom");
		assertNotNull("Failed to retrieve expected object", c);
	}
}
