package test;

import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertEquals;

import org.hibernate.AssertionFailure;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.HibernateException;

import java.util.ArrayList;
import java.util.List;

import java.lang.AssertionError;

import javax.swing.text.html.parser.Entity;

import org.testng.annotations.Test;
import org.tu.Company;
import org.tu.Course;
import org.tu.Class;
import org.tu.Instructor;
import org.tu.Student;
import org.tu.dao.EntityDAO;
import org.tu.util.SessionFactoryDispenser;
import org.testng.AssertJUnit;

@Test(groups = { "Important", "DAO" }, dependsOnGroups = { "SessionFactoryDispenser" })
public class EntityDAOTest {
	@Test
	public void saveGoodEntity() {
		EntityDAO e = new EntityDAO();
		Student stud = new Student("Artem", "Borisovich", "Chernyj", "std14achernyj", "artemchernyj2014");
		int studId = e.saveEntity(stud);
	}
	@Test(expectedExceptions = ConstraintViolationException.class)
	public void saveBadEntity() {
		EntityDAO e = new EntityDAO();
		Class cls = new Class();
		int clsId = e.saveEntity(cls);
	}
	@Test
	public void getGoodEntity() {
		EntityDAO e = new EntityDAO();
		Company c = e.getEntity(Company.class, 3);
		assertEquals("Acquired property differs from expected", "Wannaknow", c.getName());
		assertEquals("Acquired property differs from expected", "31 Spooner St.", c.getAddress());
		assertEquals("Acquired property differs from expected", "cmp14wannaknow", c.getLogin());
		assertEquals("Acquired property differs from expected", "wannaknow2014", c.getPassword());
	}
	@Test(expectedExceptions = AssertionError.class)
	public void getBadEntity() {
		EntityDAO e = new EntityDAO();
		Instructor c = (Instructor) e.getEntity(Instructor.class, 10);
		assertNotNull("Failed to retrieve expected object", c);
	}
	@Test
	public void getEntities() {
		EntityDAO e = new EntityDAO();
		ArrayList<Class> cl = new ArrayList(e.getEntities(Class.class));
		assertEquals("Number of rows in a set differs from expected", 41, cl.size());
	}
	@Test
	public void updateEntity() {
		EntityDAO e = new EntityDAO();
		Course c = e.getEntity(Course.class, 3);
		c.setDescription("Description loading, please wait...");
		e.updateEntity(c);
	}
	@Test
	public void deleteGoodEntity() {
		EntityDAO e = new EntityDAO();
		e.deleteEntity(Student.class, e.getEntity(Student.class, 3));
		Student s = e.getEntity(Student.class, 3);
	}
	@Test
	public void deleteBadEntity() {
		EntityDAO e = new EntityDAO();
		Student stud = new Student();
		e.deleteEntity(Student.class, stud);
	}
}
