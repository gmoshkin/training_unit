package test;

import static org.testng.AssertJUnit.assertEquals;

import org.hibernate.HibernateException;

import org.testng.annotations.Test;
import org.tu.Instructor;
import org.tu.Company;
import org.tu.Student;
import org.tu.dao.UserDAO;
import org.tu.util.SessionFactoryDispenser;

@Test(groups = { "Important", "DAO" }, dependsOnGroups = { "SessionFactoryDispenser" })
public class UserDAOTest {
	@Test
	public void getGoodUser() {
		UserDAO d = new UserDAO();
		Company comp = d.getUser("cmp14lotsoknowledge");
		assertEquals("Acquired property differs from expected", "Lots-o-knowledge", comp.getName());
		Student stud = d.getUser("std14mjones");
		assertEquals("Acquired property differs from expected", "Matthew", stud.getFirstName());
	}
	@Test
	public void getBadUser() {
		UserDAO d = new UserDAO();
		Instructor inst = d.getUser("goodlogin");
	}
}
