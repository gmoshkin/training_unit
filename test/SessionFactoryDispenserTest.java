package test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.testng.annotations.Test;
import org.tu.util.SessionFactoryDispenser;

@Test(groups = { "Important", "SessionFactoryDispenser" })
public class SessionFactoryDispenserTest {
	@Test
	public void getSessionFactory() {
		SessionFactory sf = SessionFactoryDispenser.getSessionFactory();
		Session session = sf.openSession();
		session.close();
	}
}
