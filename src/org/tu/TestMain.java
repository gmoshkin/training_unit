package org.tu;

import java.util.HashSet;
import java.util.List;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class TestMain {

	/**
	 * @param args
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException {
//		SessionFactory sF = new Configuration().configure().buildSessionFactory(new StandardServiceRegistryBuilder().build());
//		Session s = sF.openSession();
		Configuration cfg = new Configuration();
		cfg.configure();
		SessionFactory sF = cfg.buildSessionFactory();
		Session s = sF.openSession();
//		Session s = new Configuration().configure().buildSessionFactory(new StandardServiceRegistryBuilder().build()).getCurrentSession();
		s.beginTransaction();
		
		List<Student> l = s.createQuery("from Student").list();
		
		File file = new File("C:/Users/ion/workspace/trainingunit/out.txt");
		PrintWriter writer = new PrintWriter(file);
		for (Student stud : l) {
//		Student stud = l.get(1);
			writer.println("ID: " + stud.getId()
					+ "; FName: " + stud.getFirstName() 
					+ "; MName: " + stud.getMiddleName()
					+ "; LName: " + stud.getLastName()
					+ "; Login: " + stud.getLogin());
			writer.println('{'); 
			HashSet<Course> courses = new HashSet<Course>(stud.getCourses());
			for(Course crs : courses) {
//				for(Course crs : stud.getCourses()) { 
				writer.println("" + crs.getName() + "; "); 
			}
			writer.println('}');
		}
		TrainingUnit tu = new TrainingUnit();
		writer.println(tu.getCourses().get(1).getName());
		writer.close();
		
//		Course course = new Course();
/*		try {
			s.beginTransaction();
			s.save(stud);
			s.getTransaction().commit();
		}
		catch (Throwable e) {
			System.err.println(e);
		}
		finally {
			s.getTransaction().rollback();
		}*/
	}
}