package org.tu;

import java.io.Serializable;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class TrainingUnit {

	private SessionFactory sessionFactory;
	
	/**
	 * Constructor configures the JDBC connection and
	 * initializes the <code>SessionFactory</code>
	 */
	public TrainingUnit() {
		System.out.println("so");
		sessionFactory = new Configuration().configure().buildSessionFactory();
		System.out.println("so");
	}
	
	/**
	 * Retrieve all <code>Student</code>s from the data base.
	 * @return a <code>List</code> of <code>Student</code>s
	 */
	@SuppressWarnings("unchecked")
	public List<Student> getStudents() {
		return sessionFactory.getCurrentSession().
			createQuery("from Student s order by s.lastName, s.firstName").list();
	}

	/**
	 * Retrieve all <code>Instructor</code>s from the data base.
	 * @return a <code>List</code> of <code>Instructor</code>s
	 */
	@SuppressWarnings("unchecked")
	public List<Instructor> getInstructors() {
		return sessionFactory.getCurrentSession().
			createQuery("from Instructor i order by i.lastName, i.firstName").list();
	}

	/**
	 * Retrieve all <code>Course</code>s from the data base.
	 * @return a <code>List</code> of <code>Course</code>s
	 */
	@SuppressWarnings("unchecked")
	public List<Course> getCourses() {
		return sessionFactory.getCurrentSession().
			createQuery("from Course c order by c.name").list();
	}

	/**
	 * Retrieve all <code>Company</code>s from the data base.
	 * @return a <code>List</code> of <code>Company</code>s
	 */
	@SuppressWarnings("unchecked")
	public List<Company> getCompanies() {
		return sessionFactory.getCurrentSession().
			createQuery("from Company c order by c.name").list();
	}
	

	/**
	 * Retrieve <code>Entity</code> from the database by identity <code>identity</code>.
	 * @param <I> type of <code>identity</code>
	 * @param identity the identity to look for
	 * @return a <code>Student</code>
	 */
	@SuppressWarnings("unchecked")
	public <T extends Entity, I extends Serializable> T loadEntity(java.lang.Class<T> entityClass, I identity) {
		return (T) sessionFactory.getCurrentSession().load(entityClass, identity);
	}

	/**
	 * Retrieve <code>Student</code> from the database by identity <code>identity</code>.
	 * @param <I> type of <code>identity</code>
	 * @param identity the identity to look for
	 * @return a <code>Student</code>
	 */
//	public <I extends Serializable> Student loadStudent(I identity) {
//		return (Student) sessionFactory.getCurrentSession().load(Student.class, identity);
//	}

	/**
	 * Retrieve <code>Instructor</code> from the database by identity <code>identity</code>.
	 * @param <I> type of <code>identity</code>
	 * @param identity the identity to look for
	 * @return a <code>Instructor</code>
	 */
//	public <I extends Serializable> Instructor loadInstructor(I identity) {
//		return (Instructor) sessionFactory.getCurrentSession().load(Instructor.class, identity);
//	}

	/**
	 * Retrieve <code>Company</code> from the database by identity <code>identity</code>.
	 * @param <I> type of <code>identity</code>
	 * @param identity the identity to look for
	 * @return a <code>Company</code>
	 */
//	public <I extends Serializable> Company loadCompany(I identity) {
//		return (Company) sessionFactory.getCurrentSession().load(Company.class, identity);
//	}

	/**
	 * Retrieve <code>Course</code> from the database by identity <code>identity</code>.
	 * @param <I> type of <code>identity</code>
	 * @param identity the identity to look for
	 * @return a <code>Course</code>
	 */
//	public <I extends Serializable> Course loadCourse(I identity) {
//		return (Course) sessionFactory.getCurrentSession().load(Course.class, identity);
//	}

	/**
	 * Save an <code>Entity</code> to the data store, either inserting or updating it.
	 * @param entity the <code>Entity</code> to save
	 */
	public void storeEntity(Entity entity) {
		sessionFactory.getCurrentSession().merge(entity);
	}
}
