package org.tu.dao;

import java.lang.Class;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.tu.Entity;
import org.tu.util.SessionFactoryDispenser;

public class EntityDAO {
	
	public <T extends Entity> int saveEntity(T e) {
		SessionFactory sessionFactory = SessionFactoryDispenser.getSessionFactory();
		Session session = null;
		Integer id = null;
		try {
			session = sessionFactory.openSession();
			session.beginTransaction();
			id = (Integer) session.save(e);
			session.getTransaction().commit();
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		
		return id;
	}
	
	public <T extends Entity> void updateEntity(T e) {
		SessionFactory sessionFactory = SessionFactoryDispenser.getSessionFactory();
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.beginTransaction();
			session.update(e);
			session.getTransaction().commit();
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}
	
	public <T extends Entity> void deleteEntity(Class<T> entityClass, T entity) {
		SessionFactory sessionFactory = SessionFactoryDispenser.getSessionFactory();
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.beginTransaction();
			session.delete(entity);
			session.getTransaction().commit();
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Entity> T getEntity(Class<T> entityClass, int id) {
		SessionFactory sessionFactory = SessionFactoryDispenser.getSessionFactory();
		Session session = null;
		T entity = null;
		try {
			session = sessionFactory.openSession();
			session.beginTransaction();
			entity = (T) session.get(entityClass, id);
			session.getTransaction().commit();
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		
		return entity;
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Entity> List<T> getEntities(Class<T> entityClass) {
		SessionFactory sessionFactory = SessionFactoryDispenser.getSessionFactory();
		Session session = null;
		List<T> list = null;
		try {
			session = sessionFactory.openSession();
			session.beginTransaction();
			list = session.createCriteria(entityClass).list();
			session.getTransaction().commit();
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		
		return list;
	}
}
