package org.tu.dao;

import java.lang.String;
import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.tu.User;
import org.tu.Student;
import org.tu.Instructor;
import org.tu.Company;
import org.tu.util.SessionFactoryDispenser;


public class UserDAO {
	@SuppressWarnings("unchecked")
	public <T extends User> T getUser(String login) {
		SessionFactory sessionFactory = SessionFactoryDispenser.getSessionFactory();
		Session session = null;
		ArrayList<Student> ls = null;
		ArrayList<Instructor> li = null;
		ArrayList<Company> lc = null;
		try {
			session = sessionFactory.openSession();
			session.beginTransaction();
			ls = new ArrayList<Student>(session.createCriteria(Student.class)
					.add(Restrictions.like("login", login)).list());
			li = new ArrayList<Instructor>(session.createCriteria(Instructor.class)
					.add(Restrictions.like("login", login)).list());
			lc = new ArrayList<Company>(session.createCriteria(Company.class)
					.add(Restrictions.like("login", login)).list());
			session.getTransaction().commit();
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		
		if (ls != null && !ls.isEmpty())
			return (T) ls.get(0);

		if (li != null && !li.isEmpty())
			return (T) li.get(0);

		if (li != null && !lc.isEmpty())
			return (T) lc.get(0);
		
		return null;
	}
}
